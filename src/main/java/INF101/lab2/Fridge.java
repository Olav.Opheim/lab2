package INF101.lab2;

import java.util.NoSuchElementException;
import java.util.ArrayList;
import java.util.List;

public class Fridge implements IFridge {
    
    public ArrayList<FridgeItem> fridgeArray = new ArrayList<FridgeItem>();
    public int size = 20;

    // method that returns n amount of items in the fridge
    @Override
    public int nItemsInFridge() {
        int n = 0;
        for (int i = 0; i < fridgeArray.size(); i++) {
            if (fridgeArray.get(i) != null);
                n++;
        }
        return n;
    }

    // method that returns how much space there is left in the fridge (totalSize = size - nItemsInTheFridge())
    @Override
    public int totalSize() {
        return size;
    }

    // method that adds item in fridge and returns true if its space in the fridge, or return false if the fridge is full.
    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() == size) {
            return false;
            }
        
        else if ((nItemsInFridge() < size) && (nItemsInFridge() >= 0)) {
                fridgeArray.add(item);
                return true;
            }
        else {
            return false;
            }
        }

    // method that takes out an item from the fridge or throw NoSuchElementException if the spot is empty.
    @Override
    public void takeOut(FridgeItem item) {
        if (!fridgeArray.isEmpty()) {
            for (FridgeItem i : this.fridgeArray) {
                if (i.equals(item)) {
                    fridgeArray.remove(i);
                    break;
                }
            }
        }
        else {
            throw new NoSuchElementException();
        }
    }

    // method that empties the hole fridge
    @Override
    public void emptyFridge() {
        fridgeArray.clear();
        }

    // method that removes all expired food from the fridge and then returns a list of all expired food.
    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        
        for (int i = 0; i < nItemsInFridge(); i++) {
            FridgeItem item = fridgeArray.get(i);
            if (item.hasExpired()) {
                expiredFood.add(item);
            }
        }

        for (FridgeItem expiredItem : expiredFood) {
            fridgeArray.remove(expiredItem);
        }
        return expiredFood;
    }
}
